import os
import itertools

total_count = 0

def create_variants(input_net_filename, input_nup_filename, output_dir, target_text_prefixes, target_numbers, replacements):
    # Ensure the output directory exists
    os.makedirs(output_dir, exist_ok=True)

    # Read the content of the input .net file
    try:
        with open(input_net_filename, 'r') as file:
            net_content = file.read()
    except FileNotFoundError:
        print(f"Error: The file '{input_net_filename}' does not exist.")
        return

    # Read the content of the input .nup file
    try:
        with open(input_nup_filename, 'r') as file:
            nup_content = file.read()
    except FileNotFoundError:
        print(f"Error: The file '{input_nup_filename}' does not exist.")
        return

    # Extract the file base name and extension for .net file
    net_base_name, net_ext = os.path.splitext(os.path.basename(input_net_filename))

    # Generate all combinations of replacements
    replacement_combinations = list(itertools.product(*replacements))
    global total_count
    # Iterate over each combination of replacements
    for combination in replacement_combinations:
        modified_net_content = net_content
        modified_nup_content = nup_content
        filename_parts = []
        for prefix, number, replacement in zip(target_text_prefixes, target_numbers, combination):
            target_text = f"{prefix}{number}"
            replacement_text = f"{prefix}{replacement}"
            modified_net_content = modified_net_content.replace(target_text, replacement_text)
            modified_nup_content = modified_nup_content.replace(target_text, replacement_text)
            # Prepare part of the filename for this replacement
            filename_parts.append(replacement.replace('.', '_'))

        # Update OUTPUT_PROPAGATION_FILES and OUTPUT_NETWORK_FILES lines in .net file
        prop_replacement = f'OUTPUT_PROPAGATION_FILES "PropName_{"_".join(filename_parts)}"'
        net_replacement = f'OUTPUT_NETWORK_FILES "NetName_{"_".join(filename_parts)}"'
        modified_net_content = update_files(modified_net_content, prop_replacement, net_replacement)

        # Create new filenames for the variant .net and .nup files
        net_output_filename = os.path.join(output_dir, f'{net_base_name}_variant_{"_".join(filename_parts)}{net_ext}')
        nup_output_filename = os.path.join(output_dir, f'{net_base_name}_variant_{"_".join(filename_parts)}.nup')

        # Write the modified content to the new .net file
        with open(net_output_filename, 'w') as net_output_file:
            net_output_file.write(modified_net_content)

        # Write the modified content to the new .nup file
        with open(nup_output_filename, 'w') as nup_output_file:
            nup_output_file.write(modified_nup_content)

        print(f'Created: {net_output_filename}')
        print(f'Created: {nup_output_filename}')
        total_count +=1

def update_files(content, prop_replacement, net_replacement):
    # Search for the lines containing OUTPUT_PROPAGATION_FILES and OUTPUT_NETWORK_FILES and replace them
    lines = content.splitlines()
    for i, line in enumerate(lines):
        if line.startswith('OUTPUT_PROPAGATION_FILES'):
            lines[i] = prop_replacement
        elif line.startswith('OUTPUT_NETWORK_FILES'):
            lines[i] = net_replacement
    return '\n'.join(lines)

if __name__ == "__main__":
    # Example usage:
    input_net_filename = 'base_files/base_lte.net'  # Path to the input .net file
    input_nup_filename = 'base_files/base_lte.nup'  # Path to the input .nup file
    output_dir = '/fekoStuff/net_variants'   # Directory to store the output files
    target_text_prefixes = ['ANTENNA 1 VERTICAL ', 'ANTENNA 1 POWER ', 'ANTENNA 1 HORIZONTAL ']  # Prefixes of the text to be replaced
    target_numbers = ['5.00', '45.00000', '20.00 N']  # Numbers part of the text to be replaced

    # List of replacement numbers with specific precision
    replacements = [
        [f"{i:.2f}" for i in [x * 0.50 for x in range(17)]],      # For ANTENNA 1 VERTICAL
        [f"{i:.5f}" for i in [x + 40 for x in range(15)]],        # For ANTENNA 1 POWER
        [f"{i:.2f} N" for i in [x for x in range(0,45,5)]]          # For ANTENNA 1 HORIZONTAL
    ]
    # global total_count

    create_variants(input_net_filename, input_nup_filename, output_dir, target_text_prefixes, target_numbers, replacements)
    print("Total Count: ", total_count)