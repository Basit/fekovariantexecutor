import os
import itertools
import subprocess
import pandas as pd
import utm
import glob
import os
import numpy as np

ground_truth_df = pd.DataFrame()

def to_radians(degrees):
    return np.radians(degrees)

# Haversine distance function
def haversine(lat1, lon1, lat2, lon2):
    R = 6371000  # Radius of the Earth in meters
    dlat = to_radians(lat2 - lat1)
    dlon = to_radians(lon2 - lon1)
    a = np.sin(dlat / 2) ** 2 + np.cos(to_radians(lat1)) * np.cos(to_radians(lat2)) * np.sin(dlon / 2) ** 2
    c = 2 * np.arctan2(np.sqrt(a), np.sqrt(1 - a))
    return R * c

# Find nearest neighbors using Haversine distance
def find_nearest_neighbors(ground_truth_df, feko_df):
    nearest_indices = []
    for i, filtered_row in ground_truth_df.iterrows():
        distances = haversine(filtered_row['Latitude'], filtered_row['Longitude'], feko_df['Latitude'], feko_df['Longitude'])
        # print(distances)
        nearest_index = distances.argmin()
        if distances.min() >5:
          # print(distances.min())
          nearest_indices.append(-1)
          continue
        nearest_indices.append(nearest_index)
    return nearest_indices


def read_GR():
    global ground_truth_df
    # df1 = pd.read_csv('/fekoStuff/GR_RUN/qscan_2024-04-18_17:00:05.csv')
    # df2 = pd.read_csv('/fekoStuff/GR_RUN/qscan_2024-04-18_17:03:48.csv')
    # df3 = pd.read_csv('/fekoStuff/GR_RUN/qscan_2024-04-18_17:29:16.csv')

    # combined_df = pd.concat([df1, df2, df3], ignore_index=True)
    combined_df = pd.read_csv("/fekoStuff/fekovariantexecutor/base_files/GR_159.csv")


    ground_truth_df = combined_df.copy()
    # ground_truth_df = combined_df[combined_df['cellid'] == '6E1BB98']

    # ground_truth_df = ground_truth_df[['latitude', 'longitude', 'RSRP']]
    # ground_truth_df['Latitude'] = ground_truth_df["latitude"]
    # ground_truth_df['Longitude'] = ground_truth_df["longitude"]



    ground_truth_df = ground_truth_df.loc[~(ground_truth_df['RSRP'] == '-')]
    ground_truth_df['RSRP'] = ground_truth_df['RSRP'].astype(int)


def error_calc(feko_csv):
    global ground_truth_df
    feko_df = pd.read_csv(feko_csv)
    feko_df = feko_df.loc[~(feko_df['RSRP'] == 'N.C.')]
    feko_df['RSRP'] = feko_df['RSRP'].astype(float)
    feko_df['RSRP'] = feko_df['RSRP'].astype(int)
    nearest_indices = find_nearest_neighbors(ground_truth_df, feko_df)

    # Get the corresponding RSRP values from feko_df
    ground_truth_df_copy = ground_truth_df.copy()

  # Replace -1 indices with NaN in RSRP_feko column
    feko_values = feko_df['RSRP'].values
    RSRP_feko_values = np.where(np.array(nearest_indices) != -1, feko_values[nearest_indices], np.nan)
    ground_truth_df_copy['RSRP_feko'] = RSRP_feko_values

    # Calculate the difference in RSRP
    ground_truth_df_copy['RSRP_diff'] = ground_truth_df_copy['RSRP_feko'] - ground_truth_df_copy['RSRP']

    # Calculate the Mean Percentage Error (MPE)
    ground_truth_df_copy['RSRP_abs'] = np.abs(ground_truth_df_copy['RSRP'])
    ground_truth_df_copy['RSRP_abs_diff'] = np.abs(ground_truth_df_copy['RSRP_diff'])
    ground_truth_df_copy['MPE'] = np.abs(ground_truth_df_copy['RSRP_diff'] / ground_truth_df_copy['RSRP_abs']) * 100
    mpe = np.mean(ground_truth_df_copy['MPE'].replace([np.inf, -np.inf], np.nan).dropna())
    mae = np.mean(ground_truth_df_copy['RSRP_abs_diff'].replace([np.inf, -np.inf], np.nan).dropna())
    df = ground_truth_df_copy
    column_name = 'RSRP_abs_diff'
    median = df[column_name].median()
    lower_quartile = df[column_name].quantile(0.25)
    upper_quartile = df[column_name].quantile(0.75)
    range_val = df[column_name].max() - df[column_name].min()
    print(feko_csv, "Mean Absolute Error", mae)
    print(feko_csv, "Mean Error Rate", mpe)
    print(f"Median: {median}")
    print(f"Lower Quartile (25th percentile): {lower_quartile}")
    print(f"Upper Quartile (75th percentile): {upper_quartile}")
    print(f"Range: {range_val}")
    return mae




    


# Function to convert UTM to lat lon
def convert_to_lat_lon(x, y, zone_number, zone_letter):
    lat, lon = utm.to_latlon(x, y, zone_number, zone_letter)
    return lat, lon

# Example zone information
zone_number = 12  # Example, please replace with actual zone number
zone_letter = 'N'  # Example, please replace with actual zone letter

def csv_maker(foldername):
    input_file = f'/fekoStuff/net_variants/{foldername}/RSRP.txt'
    data_points = []
    with open(input_file, 'r') as file:
        for line in file:
            if line.strip() == 'BEGIN_DATA':
                break
        for line in file:
            if line.strip() == 'END_DATA':
                break
            parts = line.strip().split()
            x = float(parts[0])
            y = float(parts[1])
            value = parts[2]
            data_points.append((x, y, value))

    # Create output file name with parent directory name
    print(input_file)
    output_file = os.path.join(f'/fekoStuff/net_variants/{foldername}', f'RSRP.csv')
    print(output_file)
    # # Convert data points to lat lon and create DataFrame
    df_data = []
    for x, y, value in data_points:
        lat, lon = convert_to_lat_lon(x, y, zone_number, zone_letter)
        df_data.append({'Latitude': lat, 'Longitude': lon, 'RSRP': value})

    # Create DataFrame from data points
    df = pd.DataFrame(df_data)

    # Convert DataFrame to CSV
    df.to_csv(output_file, index=False)
    return output_file
def run_winprop_cli(netfile):
    """
    Execute the WinPropCLI command with the specified netfile.
    
    Parameters:
    netfile (str): The path to the netfile to be processed.
    
    Returns:
    dict: A dictionary containing 'success' (bool), 'output' (str), 'error' (str).
    """
    command = [
        '/opt/feko/2022/altair/feko/bin/WinPropCLI', 
        '-N', 
        '-P', 
        '--multi-threading', 
        '32', 
        '-F', 
        netfile
    ]
    
    try:
        # Execute the command and wait for it to complete
        result = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        
        # Return result based on execution
        return {
            'success': result.returncode == 0,
            'output': result.stdout if result.returncode == 0 else None,
            'error': result.stderr if result.returncode != 0 else None
        }
    
    except Exception as e:
        return {
            'success': False,
            'output': None,
            'error': str(e)
        }

total_count = 0

def update_horizontal_value(starting, increment):
    # Ensure starting is a list of strings, and increment is either +0.5 or -0.5
    try:
        # Parse the first value and apply the increment
        current_value = float(starting[2].split(' N')[0])
        new_value = current_value + increment
        
        # Format the new value to two decimal places
        starting[2] = f"{new_value:.2f} N"
        
        return starting
    except ValueError:
        print("Error: 3rd element in starting list is not a valid number.")
        return starting

def update_vertical_value(starting, increment):
    # Ensure starting is a list of strings, and increment is either +0.5 or -0.5
    try:
        # Parse the first value and apply the increment
        current_value = float(starting[0])
        new_value = current_value + increment
        
        # Format the new value to two decimal places
        starting[0] = f"{new_value:.2f}"
        
        return starting
    except ValueError:
        print("Error: First element in starting list is not a valid number.")
        return starting

def update_power_value(starting, increment):
    # Ensure starting is a list of strings, and increment is either +0.5 or -0.5
    try:
        # Parse the first value and apply the increment
        current_value = float(starting[1])
        new_value = current_value + increment
        
        # Format the new value to two decimal places
        starting[1] = f"{new_value:.3f}"
        
        return starting
    except ValueError:
        print("Error: Second element in starting list is not a valid number.")
        return starting

def create_single_variant(input_net_filename, input_nup_filename, output_dir, target_text_prefixes, target_numbers, replacements):
    """
    Create a single variant of .net and .nup files by replacing target texts with new values.

    Args:
        input_net_filename (str): Path to the input .net file.
        input_nup_filename (str): Path to the input .nup file.
        output_dir (str): Directory where output files will be saved.
        target_text_prefixes (list of str): Prefixes for target texts to be replaced.
        target_numbers (list of int): Numbers to combine with prefixes to form target texts.
        replacements (list of str): Replacement values for each target text.

    Returns:
        tuple: Names of the created .net and .nup files.
    """
    # Ensure the output directory exists
    os.makedirs(output_dir, exist_ok=True)

    # Read the content of the input .net file
    net_content = read_file(input_net_filename)
    if net_content is None:
        return None, None

    # Read the content of the input .nup file
    nup_content = read_file(input_nup_filename)
    if nup_content is None:
        return None, None

    # Extract the file base name and extension for .net file
    net_base_name, net_ext = os.path.splitext(os.path.basename(input_net_filename))

    modified_net_content = net_content
    modified_nup_content = nup_content
    filename_parts = []

    for prefix, number, replacement in zip(target_text_prefixes, target_numbers, replacements):
        target_text = f"{prefix}{number}"
        replacement_text = f"{prefix}{replacement}"
        modified_net_content = modified_net_content.replace(target_text, replacement_text)
        modified_nup_content = modified_nup_content.replace(target_text, replacement_text)
        # Prepare part of the filename for this replacement
        print(replacement)
        filename_parts.append(replacement.replace('.', '_'))

    # Update OUTPUT_PROPAGATION_FILES and OUTPUT_NETWORK_FILES lines in .net file
    prop_replacement = f'OUTPUT_PROPAGATION_FILES "PropName_{"_".join(filename_parts)}"'
    net_replacement = f'OUTPUT_NETWORK_FILES "NetName_{"_".join(filename_parts)}"'
    modified_net_content = update_files(modified_net_content, prop_replacement, net_replacement)

    # Create new filenames for the variant .net and .nup files
    net_output_filename = os.path.join(output_dir, f'{net_base_name}_variant_{"_".join(filename_parts)}{net_ext}')
    nup_output_filename = os.path.join(output_dir, f'{net_base_name}_variant_{"_".join(filename_parts)}.nup')

    # Write the modified content to the new .net file
    write_file(net_output_filename, modified_net_content)

    # Write the modified content to the new .nup file
    write_file(nup_output_filename, modified_nup_content)

    return net_output_filename, net_replacement.split("\"")[1]

def read_file(filename):
    """Read the content of a file."""
    try:
        with open(filename, 'r') as file:
            return file.read()
    except FileNotFoundError:
        print(f"Error: The file '{filename}' does not exist.")
        return None

def write_file(filename, content):
    """Write content to a file."""
    with open(filename, 'w') as file:
        file.write(content)

def update_files(net_content, prop_replacement, net_replacement):
    """Update specific lines in the .net file content."""
    lines = net_content.splitlines()
    updated_lines = []
    for line in lines:
        if line.startswith('OUTPUT_PROPAGATION_FILES'):
            updated_lines.append(prop_replacement)
        elif line.startswith('OUTPUT_NETWORK_FILES'):
            updated_lines.append(net_replacement)
        else:
            updated_lines.append(line)
    return '\n'.join(updated_lines)

def update_files(content, prop_replacement, net_replacement):
    # Search for the lines containing OUTPUT_PROPAGATION_FILES and OUTPUT_NETWORK_FILES and replace them
    lines = content.splitlines()
    for i, line in enumerate(lines):
        if line.startswith('OUTPUT_PROPAGATION_FILES'):
            lines[i] = prop_replacement
        elif line.startswith('OUTPUT_NETWORK_FILES'):
            lines[i] = net_replacement
    return '\n'.join(lines)

if __name__ == "__main__":
    # Example usage:
    input_net_filename = '/fekoStuff/fekovariantexecutor/base_files/base_lte_magna.net'  # Path to the input .net file
    input_nup_filename = '/fekoStuff/fekovariantexecutor/base_files/base_lte_magna.nup'  # Path to the input .nup file
    output_dir = '/fekoStuff/net_variants'   # Directory to store the output files
    target_text_prefixes = ['ANTENNA 1 VERTICAL ', 'ANTENNA 1 CARRIERS 2 CARRIER_POWER ', 'ANTENNA 1 HORIZONTAL ']  # Prefixes of the text to be replaced
    target_numbers = ['5.50', '42.000', '25.00 N']  # Numbers part of the text to be replaced

    # List of replacement numbers with specific precision

    starting = [
        '3.50',  # For ANTENNA 1 VERTICAL
        '46.000',   # For ANTENNA 1 POWER
        '25.00 N'    # For ANTENNA 1 HORIZONTAL
        ]
    
    # global total_count
    best_error = 100.0
    best_var = ""
    while True:
        read_GR()
        netfile, rsrpDir= create_single_variant(input_net_filename, input_nup_filename, output_dir, target_text_prefixes, target_numbers, starting)
        print(netfile, rsrpDir)
        run_winprop_cli(netfile)
        feko_csv = csv_maker(rsrpDir)
        error = error_calc(feko_csv)
        if error <= best_error:
            best_error = error
            best_var = netfile
        else:
            break
        # update_power_value(starting, 1)
        # update_vertical_value(starting, 0.5)
        update_horizontal_value(starting, -1)
    

    print("Best Error:", best_error )
    print("Net File", netfile)

    # update_antenna_value(starting, -0.5)
    # netfile, _ = create_single_variant(input_net_filename, input_nup_filename, output_dir, target_text_prefixes, target_numbers, starting)
    # run_winprop_cli(netfile)
    