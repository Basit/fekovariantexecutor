import os
import itertools
import subprocess
import pandas as pd
import utm
import glob
import os
import numpy as np
import json
import shutil
import re


import os
import itertools
import subprocess
import pandas as pd
import utm
import glob
import os
import numpy as np
import json
import shutil

def calculate_sinr_columns(df_temp, mode, remove_cols=None):
    df = df_temp.copy()

    # Ensure correct data types and replace None with np.nan
    df = df.astype(float).replace({None: np.nan})

    if mode == 0 and remove_cols:
        df = df[[col for col in df.columns if col not in remove_cols]]

    # Extract power columns
    power_columns = df.filter(like="Power").columns.to_list()

    if not power_columns:
        return df  # Return early if no power columns exist

    # Define thermal noise
    noise = -103.8  # Thermal noise at 10 MHz and 300K

    # Initialize a list to store SINR values
    sinr_columns = [col.replace("Power", "SINR") for col in power_columns]

    # Convert power columns to NumPy array for faster computation
    power_matrix = df[power_columns].to_numpy()

    # Count NaN values
    nan_count = np.sum(np.isnan(power_matrix))
    # print(f'Number of NaN values in power_matrix: {nan_count}')

    # Convert signal power (dBm) to linear scale
    signal_linear = np.power(10, power_matrix / 10)

    # Recount NaN values to ensure no conversions have affected them
    nan_count = np.sum(np.isnan(signal_linear))
    # print(f'Number of NaN values in signal_linear: {nan_count}')

    # Initialize SINR matrix with NaNs
    sinr_matrix = np.full_like(signal_linear, np.nan, dtype=np.float64)

    # Convert noise to linear scale
    noise_linear = 10 ** (noise / 10)

    # Loop over each power column to compute SINR
    for i, col in enumerate(power_columns):
        interference_columns = [col for j, col in enumerate(power_columns) if j != i]

        # print(f'Calculating SINR for {col}, interference columns: {interference_columns}')

        # Sum all other power values as interference while ignoring NaNs
        interference_linear = np.nansum(signal_linear[:, [power_columns.index(c) for c in interference_columns]], axis=1)

        # Add thermal noise
        interference_linear += noise_linear

        # Compute SINR (only where signal is not NaN)
        valid_idx = ~np.isnan(signal_linear[:, i])
        sinr_matrix[valid_idx, i] = signal_linear[valid_idx, i] / interference_linear[valid_idx]

    # Convert SINR values from linear scale to dB, ensuring NaNs are preserved
    sinr_db = np.full_like(sinr_matrix, np.nan, dtype=np.float64)  # Initialize with NaNs
    valid_sinr = sinr_matrix > 0  # Mask for valid SINR values
    sinr_db[valid_sinr] = 10 * np.log10(sinr_matrix[valid_sinr])  # Compute only for valid indices

    # Store the calculated SINR values back into the DataFrame
    df[sinr_columns] = sinr_db

    

    return df


def calculate_sinr_columns_opp(df_temp, mode, opp_df, remove_cols=None):
    df_orignal = df_temp.copy()
    df = merge_dataframes([df_orignal,opp_df])

    # Ensure correct data types and replace None with np.nan
    df = df.astype(float).replace({None: np.nan})

    if mode == 0 and remove_cols:
        df = df[[col for col in df.columns if col not in remove_cols]]

    # Extract power columns
    power_columns = df.filter(like="Power").columns.to_list()

    if not power_columns:
        return df  # Return early if no power columns exist

    # Define thermal noise
    noise = -103.8  # Thermal noise at 10 MHz and 300K

    # Initialize a list to store SINR values
    sinr_columns = [col.replace("Power", "SINR") for col in power_columns]

    # Convert power columns to NumPy array for faster computation
    power_matrix = df[power_columns].to_numpy()

    # Count NaN values
    nan_count = np.sum(np.isnan(power_matrix))

    # Convert signal power (dBm) to linear scale
    signal_linear = np.power(10, power_matrix / 10)

    # Recount NaN values to ensure no conversions have affected them
    nan_count = np.sum(np.isnan(signal_linear))

    # Initialize SINR matrix with NaNs
    sinr_matrix = np.full_like(signal_linear, np.nan, dtype=np.float64)

    # Convert noise to linear scale
    noise_linear = 10 ** (noise / 10)

    # Loop over each power column to compute SINR
    for i, col in enumerate(power_columns):
        interference_columns = [col for j, col in enumerate(power_columns) if j != i]


        # Sum all other power values as interference while ignoring NaNs
        interference_linear = np.nansum(signal_linear[:, [power_columns.index(c) for c in interference_columns]], axis=1)

        # Add thermal noise
        interference_linear += noise_linear

        # Compute SINR (only where signal is not NaN)
        valid_idx = ~np.isnan(signal_linear[:, i])
        sinr_matrix[valid_idx, i] = signal_linear[valid_idx, i] / interference_linear[valid_idx]

    # Convert SINR values from linear scale to dB, ensuring NaNs are preserved
    sinr_db = np.full_like(sinr_matrix, np.nan, dtype=np.float64)  # Initialize with NaNs
    valid_sinr = sinr_matrix > 0  # Mask for valid SINR values
    sinr_db[valid_sinr] = 10 * np.log10(sinr_matrix[valid_sinr])  # Compute only for valid indices

    # Store the calculated SINR values back into the DataFrame
    df[sinr_columns] = sinr_db

    
    return df

def create_single_variant(input_net_filename, input_nup_filename, output_dir, target_text_prefixes, target_numbers, replacements):
    """
    Create a single variant of .net and .nup files by replacing target texts with new values.

    Args:
        input_net_filename (str): Path to the input .net file.
        input_nup_filename (str): Path to the input .nup file.
        output_dir (str): Directory where output files will be saved.
        target_text_prefixes (list of str): Prefixes for target texts to be replaced.
        target_numbers (list of int): Numbers to combine with prefixes to form target texts.
        replacements (list of str): Replacement values for each target text.

    Returns:
        tuple: Names of the created .net and .nup files.
    """
    # Ensure the output directory exists
    os.makedirs(output_dir, exist_ok=True)

    # Read the content of the input .net file
    net_content = read_file(input_net_filename)
    if net_content is None:
        return None, None

    # Read the content of the input .nup file
    nup_content = read_file(input_nup_filename)
    if nup_content is None:
        return None, None

    # Extract the file base name and extension for .net file
    net_base_name, net_ext = os.path.splitext(os.path.basename(input_net_filename))

    modified_net_content = net_content
    modified_nup_content = nup_content
    filename_parts = []

    for prefix, number, replacement in zip(target_text_prefixes, target_numbers, replacements):
        target_text = f"{prefix}{number}"
        replacement_text = f"{prefix}{replacement}"
        # print(target_text, replacement_text)
        modified_net_content = modified_net_content.replace(target_text, replacement_text)
        modified_nup_content = modified_nup_content.replace(target_text, replacement_text)
        # Prepare part of the filename for this replacement
        # print(replacement)
        filename_parts.append(replacement.replace('.', '_'))

    # Update OUTPUT_PROPAGATION_FILES and OUTPUT_NETWORK_FILES lines in .net file
    prop_replacement = f'OUTPUT_PROPAGATION_FILES "PropNameIRT_{"_".join(filename_parts)}"'
    net_replacement = f'OUTPUT_NETWORK_FILES "NetNameIRT_{"_".join(filename_parts)}"'
    modified_net_content = update_files(modified_net_content, prop_replacement, net_replacement)

    # Create new filenames for the variant .net and .nup files
    net_output_filename = os.path.join(output_dir, f'{net_base_name}_variant_{"_".join(filename_parts)}{net_ext}')
    nup_output_filename = os.path.join(output_dir, f'{net_base_name}_variant_{"_".join(filename_parts)}.nup')

    # Write the modified content to the new .net file
    write_file(net_output_filename, modified_net_content)

    # Write the modified content to the new .nup file
    write_file(nup_output_filename, modified_nup_content)

    return net_output_filename, net_replacement.split("\"")[1]
def read_file(filename):
    """Read the content of a file."""
    try:
        with open(filename, 'r') as file:
            return file.read()
    except FileNotFoundError:
        print(f"Error: The file '{filename}' does not exist.")
        return None

def write_file(filename, content):
    """Write content to a file."""
    with open(filename, 'w') as file:
        file.write(content)

def update_files(net_content, prop_replacement, net_replacement):
    """Update specific lines in the .net file content."""
    lines = net_content.splitlines()
    updated_lines = []
    for line in lines:
        if line.startswith('OUTPUT_PROPAGATION_FILES'):
            updated_lines.append(prop_replacement)
        elif line.startswith('OUTPUT_NETWORK_FILES'):
            updated_lines.append(net_replacement)
        else:
            updated_lines.append(line)
    return '\n'.join(updated_lines)

def extract_antenna_data(filename, cellid):
    with open(filename, 'r') as file:
        lines = file.readlines()

    antenna_data = {}
    prefixes = []
    values = []
    
    # Define regex patterns to match antenna number and properties
    antenna_pattern = re.compile(r'ANTENNA (\d+) NAME "([A-Za-z0-9\s\-]+)"')  # Match ANTENNA number and NAME
    vertical_pattern = re.compile(r'ANTENNA (\d+) VERTICAL (\d+\.\d+)')
    horizontal_pattern = re.compile(r'ANTENNA (\d+) HORIZONTAL (\d+\.\d+ [A-Z])')  # Include 'N' in the pattern
    carrier_power_pattern = re.compile(r'ANTENNA (\d+) CARRIERS 1 CARRIER_POWER (\d+\.\d+)')

    # Loop through lines to capture all antennas for the given cellid
    current_antenna = None
    for line in lines:
        # Look for the antenna and its name first
        antenna_match = antenna_pattern.search(line)
        if antenna_match:
            antenna_number = antenna_match.group(1)
            antenna_name = antenna_match.group(2)
            
            if cellid in antenna_name:
                current_antenna = antenna_number  # Update current antenna number based on cellid match

        # Once we've identified the antenna number, gather its properties
        if current_antenna:
            # Find vertical, horizontal, and carrier power for the identified antenna
            vertical_match = vertical_pattern.search(line)
            horizontal_match = horizontal_pattern.search(line)
            carrier_power_match = carrier_power_pattern.search(line)

            # Store matches in the dictionary if they exist
            if vertical_match and vertical_match.group(1) == current_antenna:
                prefixes.append(f'ANTENNA {current_antenna} VERTICAL ')
                values.append(vertical_match.group(2))
            if horizontal_match and horizontal_match.group(1) == current_antenna:
                prefixes.append(f'ANTENNA {current_antenna} HORIZONTAL ')
                values.append(horizontal_match.group(2))
            if carrier_power_match and carrier_power_match.group(1) == current_antenna:
                prefixes.append(f'ANTENNA {current_antenna} CARRIERS 1 CARRIER_POWER ')
                values.append(carrier_power_match.group(2))
                prefixes.append(f'ANTENNA {current_antenna} DISABLED ')
                values.append("y")


    return prefixes, values



def run_winprop_cli(netfile):
    """
    Execute the WinPropCLI command with the specified netfile.
    
    Parameters:
    netfile (str): The path to the netfile to be processed.
    
    Returns:
    dict: A dictionary containing 'success' (bool), 'output' (str), 'error' (str).
    """
    command = [
        '/opt/feko/2022/altair/feko/bin/WinPropCLI', 
        '-A', 
        # '-P', 
        '--multi-threading', 
        '32', 
        '-F', 
        netfile
    ]
    
    try:
        # Execute the command and wait for it to complete
        result = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=False)  # text=False ensures raw byte output
        
        # Decode output using a different encoding (ISO-8859-1) to handle potential encoding issues
        stdout_output = result.stdout.decode('ISO-8859-1', errors='replace')  # Use 'replace' to handle invalid characters
        stderr_output = result.stderr.decode('ISO-8859-1', errors='replace')
        
        # Return result based on execution
        return {
            'success': result.returncode == 0,
            'output': stdout_output if result.returncode == 0 else None,
            'error': stderr_output if result.returncode != 0 else None
        }
    
    except Exception as e:
        return {
            'success': False,
            'output': None,
            'error': str(e)
        }


def make_df(file_path):
    location = file_path.split('/')[-1].split(' Power')[0]
    print(location)
    data = []
    with open(file_path, 'r') as file:
        begin_data_found = False
        for line in file:
            # print(line)
            if begin_data_found:
                try:
                    # Assuming data is space-separated, adjust as needed
                    lat, lon, power = line.strip().split()
                    if power == 'N.C.':
                        power = np.nan
                        data.append([float(lat), float(lon), (power)])
                    else:
                        data.append([float(lat), float(lon), float(power)])
                except ValueError:
                    # Handle lines that don't match the expected format
                    pass
                    # Or you might choose to raise an error here
            elif line.strip() == 'BEGIN_DATA':
                begin_data_found = True
    if data:
        return pd.DataFrame(data, columns=['Latitude', 'Longitude', location+ ' Power'])
    else:
        return None

def make_power_df(directory):
    txt_files = glob.glob(f"{directory}/*/*Power.txt")
    df_list = []
    for txt_file in txt_files:
        print(txt_file)
        df = make_df(txt_file)
        df_list.append(df)
    final_merged_df = merge_dataframes(df_list)
    return final_merged_df

def merge_sinr_dataframes(df1_temp, df2_temp, first, sec):
    # Rename columns in df2 by appending '_2' to each MEB-south related column
    merged_df = pd.DataFrame()
    df1 = df1_temp.copy()
    df2 = df2_temp.copy()
    # Merge the dataframes on Latitude and Longitude
    merged_df = pd.merge(df1, df2, on=['Latitude', 'Longitude'], suffixes=(f'_{first}', f'_{sec}'))
    return merged_df

def merge_dataframes(df_list):
    merged_df = df_list[0]
    # Iterate through the remaining DataFrames and merge
    for i in range(1, len(df_list)):
        # Merge based on Latitude and Longitude, adding suffixes to avoid column name collisions
        merged_df = pd.merge(merged_df, df_list[i], on=['Latitude', 'Longitude'],
                             suffixes=(f'_{i}', f'_{i+1}'))

    return merged_df


def calculate_sinr(signal, interference_powers):
    noise = -103.8 #thermal noise at 10 Mhz and 300k
    interference_powers.append(noise)
    signal_linear = 10 ** (signal / 10)
    interference_linear = sum(10 ** (np.array(interference_powers) / 10))

    # # Avoid log of zero or negative values
    # if interference_linear == 0:
    #     return np.inf  # Infinite SINR if no interference
    sinr_linear = signal_linear / interference_linear

    sinr_db = 10 * np.log10(sinr_linear)
    return sinr_db

def make_sinr_df(directory, meb_rsrp_path, was_rsrp_path ):
    txt_files = glob.glob(f"{directory}/*Power.txt")
    df_list = []
    for txt_file in txt_files:
        df = make_df(txt_file)
        if df is not None:
            df_list.append(df)

    rsrp_df_1 = make_rsrp_df(meb_rsrp_path)
    df_list.append(rsrp_df_1)
    rsrp_df_2 = make_rsrp_df(was_rsrp_path)
    df_list.append(rsrp_df_2)
    final_merged_df = merge_dataframes(df_list)
    df = final_merged_df.copy()
    # Calculate SINR for all Power columns
    sinr_df_comm = calculate_sinr_columns(df, 0)
    sinr_df_opp = calculate_sinr_columns(df, 1)
    sinr_df_all = calculate_sinr_columns(df, 2)
    sinr_df_sleep = calculate_sinr_columns(df, 3)
    return sinr_df_comm, sinr_df_opp, sinr_df_all, sinr_df_sleep

def make_opp_sinr_diff_plot_with_rsrp(cellname, power_df, opp_df, rsrp_9820_df_temp, rsrp_1100_df_temp, sleeping_cells):
    rsrp_9820_df = rsrp_9820_df_temp.copy()
    rsrp_1100_df = rsrp_1100_df_temp.copy()
    opp_power_list = [f"{cellname} Power"]
    opp_sinr_list = [f"{cellname} SINR"]

    sinr_df_before = calculate_sinr_columns(power_df, 1)
    sinr_df_after = calculate_sinr_columns_opp(power_df, 0, opp_df, sleeping_cells)
    sleeping_cells_sinr = [col.replace("Power", "SINR") for col in sleeping_cells]
    sleeping_cells_rsrp = [col.replace("Power", "RSRP") for col in sleeping_cells]

    print("SINR Before: ", len(sinr_df_before.columns))
    print("SINR After: ", len(sinr_df_after.columns))

    sinr_df_before = sinr_df_before.drop(columns=sleeping_cells_sinr)
    sinr_df_after = sinr_df_after.drop(columns=opp_sinr_list)

    print("SINR Before: ", len(sinr_df_before.columns))
    print("SINR After: ", len(sinr_df_after.columns))
    sinr_df_before['Max SINR'] = sinr_df_before.iloc[:, 2:].max(axis=1, skipna=True)
    sinr_df_after['Max SINR'] = sinr_df_after.iloc[:, 2:].max(axis=1, skipna=True)
    # sinr_merged = merge_sinr_dataframes(sinr_df_before, sinr_df_after, 'Before', 'After')
    # sinr_merged['SINR'] = sinr_merged['Max SINR_After'] - sinr_merged['Max SINR_Before']
    sinr_diff = sinr_df_after['Max SINR'] - sinr_df_before['Max SINR']
    sinr_diff_df = pd.DataFrame({
        'Latitude': sinr_df_before['Latitude'],
        'Longitude': sinr_df_before['Longitude'],
        'SINR': sinr_diff
    })

    rsrp_9820_df = rsrp_9820_df.drop(columns=sleeping_cells_rsrp)

    rsrp_9820_df['Max RSRP'] = rsrp_9820_df.iloc[:, 2:].max(axis=1, skipna=True)
    rsrp_1100_df['Max RSRP'] = rsrp_1100_df.iloc[:, 2:].max(axis=1, skipna=True)
    combined_df = merge_sinr_dataframes(rsrp_1100_df, rsrp_9820_df, '1100', '9820')
    combined_df["RSRP Diff"] = combined_df["Max RSRP_1100"] - combined_df["Max RSRP_9820"]
    combined_df_sinr = merge_sinr_dataframes(combined_df, sinr_diff_df, 'Before', 'After')

    # combined_df_sinr.loc[combined_dfv  _sinr["RSRP Diff"] > 15, combined_df_sinr.filter(like="SINR").columns] = 5.1
    print(len(combined_df_sinr))
    combined_df_sinr.drop(combined_df_sinr[combined_df_sinr["Max RSRP_9820"]< -116].index, inplace=True)
    print(len(combined_df_sinr))
    combined_df_sinr.drop(combined_df_sinr[combined_df_sinr["RSRP Diff"] > 15].index, inplace=True)
    print(len(combined_df_sinr))




    return combined_df_sinr

def update_power_value(starting, increment):
    # Ensure starting is a list of strings, and increment is either +0.5 or -0.5
    try:
        # Parse the first value and apply the increment
        current_value = float(starting[2])
        new_value = current_value + increment
        
        # Format the new value to two decimal places
        starting[2] = f"{new_value:.3f}"
        
        return starting
    except ValueError:
        print("Error: Third element in starting list is not a valid number.")
        return starting

def run_single(starting, cellid, input_net_filename, input_nup_filename, output_dir, target_text_prefixes, target_numbers):
    print("Current Run:", starting)
    
    netfile, rsrpDir = create_single_variant(input_net_filename, input_nup_filename, output_dir, target_text_prefixes, target_numbers, starting)
    print(netfile, rsrpDir)
    errors = {'error': None, 'success': False}
    
    # while errors['success'] == False:
    errors = run_winprop_cli(netfile)
        # print(errors['success'], erroxrs['error'])

   

def run_single_cell(cellid, starting):
    input_net_filename = '/fekoStuff/fekovariantexecutor/base_files/base_IRT_uofu_ATT.net'
    input_nup_filename = '/fekoStuff/fekovariantexecutor/base_files/base_IRT_uofu_ATT.nup'
    output_dir = f'/fekoStuff/net_variants_single_{cellid}'
    if os.path.exists(output_dir):
        shutil.rmtree(output_dir)
    target_text_prefixes, target_numbers = extract_antenna_data(input_net_filename, cellid)
    target_text_prefixes.extend(['EXPONENT_BEFORE_BREAKPOINT ', 'EXPONENT_BEFORE_BREAKPOINT_LOS '])
    target_numbers.extend(['2.700', '2.500'])
    run_single(starting, cellid, input_net_filename, input_nup_filename, output_dir, target_text_prefixes, target_numbers)
    return output_dir


def main():
    power_df = pd.read_csv("power_df.csv", index_col=0)
    rsrp_df = pd.read_csv("rsrp_df.csv", index_col=0)
    rsrp_df_1100 = pd.read_csv("rsrp_df_1100.csv", index_col=0)
    max_power = 30
    # cell = ['EBC-DD1', ['0.00 N', '0.00', '10.000', 'n', '2.700', '2.500']]
    # cell = ['Honors-1', ['337.50 N', '0.00', '10.000', 'n', '2.700', '2.500']]
    cell = ['Browning-1', ['135.50 N', '0.00', '10.000', 'n', '2.700', '2.500']]
    impact_dict = {}
    file_path = f'/fekoStuff/fekovariantexecutor/impact_results.json'
    try:
        with open(file_path, 'r') as file:
            impact_dict = json.load(file)
    except FileNotFoundError:
        impact_dict = {}
    while max_power >= int(float(cell[1][2])):
        print(cell)
        if str(cell) in impact_dict:
            print("Already Calculated")
            cell[1] = update_power_value(cell[1],1)
            continue
        output_dir = run_single_cell(cell[0],cell[1])
        opp_power_df = make_power_df(output_dir)
        opp_diff_df_all = make_opp_sinr_diff_plot_with_rsrp(cell[0], power_df, opp_power_df, rsrp_df, rsrp_df_1100,['6E1BB98 167 Power','6E22695 309 Power','6E22697 311 Power'])
        print(opp_diff_df_all['SINR'].describe())
        opp_diff_df_all_filtered = opp_diff_df_all[opp_diff_df_all['SINR']<-1]
        print(len(opp_diff_df_all_filtered))
        impact_dict[str(cell)] = len(opp_diff_df_all_filtered)
        with open(file_path, 'w') as file:
            json.dump(impact_dict.copy(), file, indent=4)
        cell[1] = update_power_value(cell[1],1)




if __name__ == "__main__":
    main()