import pandas as pd
import utm
import glob
import os

# Function to convert UTM to lat lon
def convert_to_lat_lon(x, y, zone_number, zone_letter):
    lat, lon = utm.to_latlon(x, y, zone_number, zone_letter)
    return lat, lon

# Example zone information
zone_number = 12  # Example, please replace with actual zone number
zone_letter = 'N'  # Example, please replace with actual zone letter

# Glob pattern to match input files
# input_files = glob.glob('/fekoStuff/net_variants/*/RSRP.txt')
input_files = ['/fekoStuff/net_variants/NetName_5_50_42_00000_25_00 N/RSRP.txt']
for input_file in input_files:
    # Read data from file and extract data points
    data_points = []
    with open(input_file, 'r') as file:
        for line in file:
            if line.strip() == 'BEGIN_DATA':
                break
        for line in file:
            if line.strip() == 'END_DATA':
                break
            parts = line.strip().split()
            x = float(parts[0])
            y = float(parts[1])
            value = parts[2]
            data_points.append((x, y, value))

    # Extract parent directory name
    parent_directory_name = os.path.basename(os.path.dirname(input_file))
    # Create output file name with parent directory name
    print(input_file)
    output_file = os.path.join("/fekoStuff/computed_rsrp", f'RSRP_{parent_directory_name}.csv')
    print(output_file)
    # # Convert data points to lat lon and create DataFrame
    df_data = []
    for x, y, value in data_points:
        lat, lon = convert_to_lat_lon(x, y, zone_number, zone_letter)
        df_data.append({'Latitude': lat, 'Longitude': lon, 'Value': value})

    # Create DataFrame from data points
    df = pd.DataFrame(df_data)

    # Convert DataFrame to CSV
    df.to_csv(output_file, index=False)

print("CSV files saved successfully.")
