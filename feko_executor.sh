#!/bin/bash

output_dir="/fekoStuff/net_variants"

# Execute command on each variant file
for file in "$output_dir"/*.net; do
    /opt/feko/2022/altair/feko/bin/WinPropCLI -N -P --multi-threading 32 -F "$file"
done