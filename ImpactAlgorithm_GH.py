import os
import itertools
import subprocess
import pandas as pd
import utm
import glob
import os
import numpy as np
import json
import shutil

def merge_sinr_dataframes(df1_temp, df2_temp, first, sec):
    # Rename columns in df2 by appending '_2' to each MEB-south related column
    merged_df = pd.DataFrame()
    df1 = df1_temp.copy()
    df2 = df2_temp.copy()
    # Merge the dataframes on Latitude and Longitude
    merged_df = pd.merge(df1, df2, on=['Latitude', 'Longitude'], suffixes=(f'_{first}', f'_{sec}'))
    return merged_df

def merge_dataframes(df_list):
    merged_df = df_list[0]
    # Iterate through the remaining DataFrames and merge
    for i in range(1, len(df_list)):
        # Merge based on Latitude and Longitude, adding suffixes to avoid column name collisions
        merged_df = pd.merge(merged_df, df_list[i], on=['Latitude', 'Longitude'], 
                             suffixes=(f'_{i}', f'_{i+1}')) 

    return merged_df

def make_rsrp_df(file_path):
    location = ""
    data = []
    with open(file_path, 'r') as file:
        begin_data_found = False
        for line in file:
            # print(line)
            if line.startswith("ANTENNA 1\tNAME"):
                # Extract the name as the location
                location = line.strip().split("\t")[-1]
            if begin_data_found:
                try:
                    # Assuming data is space-separated, adjust as needed
                    lat, lon, power = line.strip().split()
                    if power == 'N.C.':
                        power = np.nan  
                        data.append([float(lat), float(lon), (power)])  
                    else:
                        data.append([float(lat), float(lon), float(power)])
                except ValueError:
                    # Handle lines that don't match the expected format
                    pass
                    # Or you might choose to raise an error here
            elif line.strip() == 'BEGIN_DATA':
                begin_data_found = True
    if data:
        return pd.DataFrame(data, columns=['Latitude', 'Longitude', location+ ' RSRP'])
    else:
        return None
def make_df(file_path):
    location = file_path.split('/')[-1].split(' Power')[0]
    print(location)
    data = []
    with open(file_path, 'r') as file:
        begin_data_found = False
        for line in file:
            # print(line)
            if begin_data_found:
                try:
                    # Assuming data is space-separated, adjust as needed
                    lat, lon, power = line.strip().split()
                    if power == 'N.C.':
                        power = np.nan  
                        data.append([float(lat), float(lon), (power)])  
                    else:
                        data.append([float(lat), float(lon), float(power)])
                except ValueError:
                    # Handle lines that don't match the expected format
                    pass
                    # Or you might choose to raise an error here
            elif line.strip() == 'BEGIN_DATA':
                begin_data_found = True
    if data:
        return pd.DataFrame(data, columns=['Latitude', 'Longitude', location+ ' Power'])
    else:
        return None
def calculate_sinr(signal, interference_powers):
    noise = -103.8 #thermal noise at 10 Mhz and 300k
    interference_powers.append(noise)
    signal_linear = 10 ** (signal / 10)
    interference_linear = sum(10 ** (np.array(interference_powers) / 10))
    
    # # Avoid log of zero or negative values
    # if interference_linear == 0:
    #     return np.inf  # Infinite SINR if no interference
    sinr_linear = signal_linear / interference_linear
    
    sinr_db = 10 * np.log10(sinr_linear)
    return sinr_db
def calculate_sinr_columns(df_temp, mode):
    df = df_temp.copy()
    df = df_temp.copy()
    if mode == 0:
        df = df[[col for col in df.columns if "GH-DD1" not in col]] # normal commercial transmitting
        # df = df[[col for col in df.columns if "EBC-DD1" not in col]] # normal commercial transmitting
    if mode == 1:
        df = df[[col for col in df.columns if "USTAR-DD1" not in col]] # oppurtunistic transmitting
    if mode == 2:
        df = df # all transmitting
    if mode == 3:
        #only non sleeping commercial are transmitting 
        df = df[[col for col in df.columns if "USTAR-DD1" not in col]]
        df = df[[col for col in df.columns if "GH-DD1" not in col]] 
        # df = df[[col for col in df.columns if "EBC-DD1" not in col]] 

    power_columns = [col for col in df.columns if "Power" in col]
    for col in power_columns:
        # Define interference columns as all except the current one
        interference_columns = [c for c in power_columns if c != col]
        df[col.replace("Power","SINR")] = df.apply(
            lambda row: calculate_sinr(
                row[col], 
                [row[interference] for interference in interference_columns]
            ), 
            axis=1
        )
    return df

def make_sinr_df(directory, meb_rsrp_path, was_rsrp_path ):
    txt_files = glob.glob(f"{directory}/*Power.txt")
    df_list = []
    for txt_file in txt_files:
        df = make_df(txt_file)
        if df is not None:
            df_list.append(df)

    rsrp_df_1 = make_rsrp_df(meb_rsrp_path)
    df_list.append(rsrp_df_1)
    rsrp_df_2 = make_rsrp_df(was_rsrp_path)
    df_list.append(rsrp_df_2)
    final_merged_df = merge_dataframes(df_list)
    df = final_merged_df.copy()
    # Calculate SINR for all Power columns
    sinr_df_comm = calculate_sinr_columns(df, 0)
    sinr_df_opp = calculate_sinr_columns(df, 1)
    sinr_df_all = calculate_sinr_columns(df, 2)
    sinr_df_sleep = calculate_sinr_columns(df, 3)
    return sinr_df_comm, sinr_df_opp, sinr_df_all, sinr_df_sleep

def calcdiff(df1_temp, df2_temp, first, sec, thres):
    df1 = df1_temp.copy()
    df2 = df2_temp.copy()
    # Call the function
    final_df = merge_sinr_dataframes(df1, df2, first, sec)
    column_names = final_df.columns
    print(column_names)
    final_df_meb = final_df.copy()
    final_df_was = final_df.copy()
    # Get diff for meb after applying threshold
    final_df_meb[f'MEB-south RSRP_{first}'] = final_df_meb[f'MEB-south RSRP_{first}'].astype(float)
    final_df_meb = final_df_meb[final_df_meb[f'MEB-south RSRP_{first}']>=thres]
    print(final_df_meb[f'MEB-south RSRP_{first}'].describe())
    print("meb Length=", len(final_df_meb))
    final_df_meb['MEB-south SINR_diff'] = final_df_meb[f'MEB-south SINR_{first}'].astype(float) - final_df_meb[f'MEB-south SINR_{sec}'].astype(float)
    # Get diff for wasatch after applying threshold
    final_df_was[f'Wasatch-DD1 RSRP_{first}'] = final_df_was[f'Wasatch-DD1 RSRP_{first}'].astype(float)
    final_df_was = final_df_was[final_df_was[f'Wasatch-DD1 RSRP_{first}']>=thres]
    print(final_df_was[f'Wasatch-DD1 RSRP_{first}'].describe())
    print("Was Length=", len(final_df_was))
    final_df_was['Wasatch-DD1 SINR_diff'] = final_df_was[f'Wasatch-DD1 SINR_{first}'].astype(float) - final_df_was[f'Wasatch-DD1 SINR_{sec}'].astype(float)
    
    final_df_was = final_df_was[final_df_was['Wasatch-DD1 SINR_diff']>=0]
    final_df_meb = final_df_meb[final_df_meb['MEB-south SINR_diff']>=0]
    return final_df_meb['MEB-south SINR_diff'].mean(), final_df_was['Wasatch-DD1 SINR_diff'].mean(), len(final_df_meb), len(final_df_was)
# Power columns in the dataset
def calcDiffMean(directory, rsrpDir1, rsrpDir2):
    # Apply SINR calculation row-wise
    fullDir = f"/fekoStuff/net_variants/{directory}"
    fullDirRsrp1 = f"/fekoStuff/net_variants/{rsrpDir1}/RSRP.txt"
    fullDirRsrp2 = f"/fekoStuff/net_variants/{rsrpDir2}/RSRP.txt"
    
    sinr_df_comm, sinr_df_opp, sinr_df_all, sinr_df_sleep = make_sinr_df(fullDir,fullDirRsrp1,fullDirRsrp2)
    sinr_df_comm.to_csv(fullDir+"/commercial.csv")
    sinr_df_opp.to_csv(fullDir+"/opp.csv")
    sinr_df_all.to_csv(fullDir+"/all.csv")
    sinr_df_sleep.to_csv(fullDir+"/sleep.csv")
    return calcdiff(sinr_df_comm, sinr_df_opp, 'comm', 'opp', -112.0)


def run_winprop_cli(netfile):
    """
    Execute the WinPropCLI command with the specified netfile.
    
    Parameters:
    netfile (str): The path to the netfile to be processed.
    
    Returns:
    dict: A dictionary containing 'success' (bool), 'output' (str), 'error' (str).
    """
    command = [
        '/opt/feko/2022/altair/feko/bin/WinPropCLI', 
        '-N', 
        '-P', 
        '--multi-threading', 
        '32', 
        '-F', 
        netfile
    ]
    
    try:
        # Execute the command and wait for it to complete
        result = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        
        # Return result based on execution
        return {
            'success': result.returncode == 0,
            'output': result.stdout if result.returncode == 0 else None,
            'error': result.stderr if result.returncode != 0 else None
        }
    
    except Exception as e:
        return {
            'success': False,
            'output': None,
            'error': str(e)
        }

total_count = 0



def update_power_value(starting, increment, antenna):
    # Ensure starting is a list of strings, and increment is either +0.5 or -0.5
    try:
        # Parse the first value and apply the increment
        current_value = float(starting[antenna])
        new_value = current_value + increment
        
        # Format the new value to two decimal places
        starting[antenna] = f"{new_value:.3f}"
        
        return starting
    except ValueError:
        print("Error: Second element in starting list is not a valid number.")
        return starting



def create_single_variant(input_net_filename, input_nup_filename, output_dir, target_text_prefixes, target_numbers, replacements, mode):
    """
    Create a single variant of .net and .nup files by replacing target texts with new values.

    Args:
        input_net_filename (str): Path to the input .net file.
        input_nup_filename (str): Path to the input .nup file.
        output_dir (str): Directory where output files will be saved.
        target_text_prefixes (list of str): Prefixes for target texts to be replaced.
        target_numbers (list of int): Numbers to combine with prefixes to form target texts.
        replacements (list of str): Replacement values for each target text.
        mode which mode to run it at: 0 all transmitting, 1 only meb transmitting, 2 only wasatch transmitting

    Returns:
        tuple: Names of the created .net and .nup files.
    """
    # Ensure the output directory exists
    os.makedirs(output_dir, exist_ok=True)
    target_text_prefixes = target_text_prefixes.copy()
    target_numbers = target_numbers.copy()
    replacements = replacements.copy()
    filename_parts = []
    if mode ==1 or mode ==2:
        input_net_filename = input_net_filename.split(".")[0] + "_disabled" + ".net"
        input_nup_filename = input_nup_filename.split(".")[0] + "_disabled" + ".nup"
        if mode == 1:
            target_text_prefixes.append("ANTENNA 7 DISABLED ")
            target_numbers.append("y")
            replacements.append("n")
        if mode == 2:
            target_text_prefixes.append("ANTENNA 10 DISABLED ")
            target_numbers.append("y")
            replacements.append("n")
    # Read the content of the input .net file
    net_content = read_file(input_net_filename)
    if net_content is None:
        return None, None

    # Read the content of the input .nup file
    nup_content = read_file(input_nup_filename)
    if nup_content is None:
        return None, None

    # Extract the file base name and extension for .net file
    net_base_name, net_ext = os.path.splitext(os.path.basename(input_net_filename))

    modified_net_content = net_content
    modified_nup_content = nup_content
    # filename_parts = []

    for prefix, number, replacement in zip(target_text_prefixes, target_numbers, replacements):
        target_text = f"{prefix}{number}"
        replacement_text = f"{prefix}{replacement}"
        modified_net_content = modified_net_content.replace(target_text, replacement_text)
        modified_nup_content = modified_nup_content.replace(target_text, replacement_text)
        # Prepare part of the filename for this replacement
        filename_parts.append(replacement.replace('.', '_'))
    if mode==1:
        filename_parts.append("1")
    if mode==2:
        filename_parts.append("2")
    # Update OUTPUT_PROPAGATION_FILES and OUTPUT_NETWORK_FILES lines in .net file
    prop_replacement = f'OUTPUT_PROPAGATION_FILES "PropNameIRT_{"_".join(filename_parts)}"'
    net_replacement = f'OUTPUT_NETWORK_FILES "NetNameIRT_{"_".join(filename_parts)}"'
    modified_net_content = update_files(modified_net_content, prop_replacement, net_replacement)

    # Create new filenames for the variant .net and .nup files
    net_output_filename = os.path.join(output_dir, f'{net_base_name}_variant_{"_".join(filename_parts)}{net_ext}')
    nup_output_filename = os.path.join(output_dir, f'{net_base_name}_variant_{"_".join(filename_parts)}.nup')

    # Write the modified content to the new .net file
    write_file(net_output_filename, modified_net_content)

    # Write the modified content to the new .nup file
    write_file(nup_output_filename, modified_nup_content)
    if mode ==1 or mode ==2:
        return net_output_filename, net_replacement.split("\"")[1]
    else:
        return net_output_filename, prop_replacement.split("\"")[1]

def read_file(filename):
    """Read the content of a file."""
    try:
        with open(filename, 'r') as file:
            return file.read()
    except FileNotFoundError:
        print(f"Error: The file '{filename}' does not exist.")
        return None

def write_file(filename, content):
    """Write content to a file."""
    with open(filename, 'w') as file:
        file.write(content)

def update_files(net_content, prop_replacement, net_replacement):
    """Update specific lines in the .net file content."""
    lines = net_content.splitlines()
    updated_lines = []
    for line in lines:
        if line.startswith('OUTPUT_PROPAGATION_FILES'):
            updated_lines.append(prop_replacement)
        elif line.startswith('OUTPUT_NETWORK_FILES'):
            updated_lines.append(net_replacement)
        else:
            updated_lines.append(line)
    return '\n'.join(updated_lines)



def runAllVariants():
    input_net_filename = '/fekoStuff/fekovariantexecutor/base_files/base_IRT_uofu.net'  # Path to the input .net file
    input_nup_filename = '/fekoStuff/fekovariantexecutor/base_files/base_IRT_uofu.nup'  # Path to the input .nup file
    output_dir = '/fekoStuff/net_variants'   # Directory to store the output files
    target_text_prefixes = ['ANTENNA 7 CARRIERS 1 CARRIER_POWER ', 'ANTENNA 10 CARRIERS 1 CARRIER_POWER ', 'ANTENNA 3 CARRIERS 1 CARRIER_POWER ']  # Prefixes of the text to be replaced
    # target_text_prefixes = ['ANTENNA 7 CARRIERS 1 CARRIER_POWER ', 'ANTENNA 10 CARRIERS 1 CARRIER_POWER ', 'ANTENNA 2 CARRIERS 1 CARRIER_POWER ']  # Prefixes of the text to be replaced
    target_numbers = ['30.000', '30.000', '30.000']  # Numbers part of the text to be replaced

    starting_initial = [
        '20.000',  # For Meb-south
        '20.000',   # For Wasatch
        '10.000'    # For ebc
        ]
    starting = [
        '20.000',  # For Meb-south
        '20.000',   # For Wasatch
        '10.000'    # For ebc
        ]
    ending = [
        '45.000',  # For Meb-south
        '45.000',   # For Wasatch
        '40.000'    # For ebc
        ]
    

    std_dev_dict = {}
    file_path = 'antenna_powers_withRSRP_GH.json'
    try:
        with open(file_path, 'r') as file:
            std_dev_dict = json.load(file)
    except FileNotFoundError:
        std_dev_dict = {}
    while starting[0]!=ending[0]:
        while starting[1] != ending[1]:
            while starting[2] != ending[2]:
                print("Current Run:", starting)
                if str(starting) in  std_dev_dict:
                    print("Already Calculated:",str(starting))
                    update_power_value(starting, 5, 2 )
                    continue
                netfile, powerDir= create_single_variant(input_net_filename, input_nup_filename, output_dir, target_text_prefixes, target_numbers, starting,0)
                print(netfile, powerDir)
                errors = run_winprop_cli(netfile)
                netfile1, rsrpDir1= create_single_variant(input_net_filename, input_nup_filename, output_dir, target_text_prefixes, target_numbers, starting,1)
                print(netfile1, rsrpDir1)
                errors = run_winprop_cli(netfile1)
                netfile2, rsrpDir2= create_single_variant(input_net_filename, input_nup_filename, output_dir, target_text_prefixes, target_numbers, starting,2)
                print(netfile2, rsrpDir2)
                errors = run_winprop_cli(netfile2)
                means = calcDiffMean(powerDir, rsrpDir1, rsrpDir2)
                print(means)
                # exit(0)
                try:
                    with open(file_path, 'r') as file:
                        std_dev_dict = json.load(file)
                        print("File read successfully")
                except FileNotFoundError:
                    print("File Error")
                std_dev_dict[str(starting)] = (means)
                with open(file_path, 'w') as file:
                    json.dump(std_dev_dict, file, indent=4)
                update_power_value(starting, 5, 2 )
            starting[2] = starting_initial[2]
            update_power_value(starting, 5, 1 )

            # print("Deleting Direcoty")
            try:
                # shutil.rmtree(output_dir)
                pass
            except:
                print("Directory not present")
        starting[1] = starting_initial[1]
        update_power_value(starting, 5, 0 )
        
def runVariant(starting):
    input_net_filename = '/fekoStuff/fekovariantexecutor/base_files/base_IRT_uofu.net'  # Path to the input .net file
    input_nup_filename = '/fekoStuff/fekovariantexecutor/base_files/base_IRT_uofu.nup'  # Path to the input .nup file
    output_dir = '/fekoStuff/net_variants'   # Directory to store the output files
    target_text_prefixes = ['ANTENNA 7 CARRIERS 1 CARRIER_POWER ', 'ANTENNA 10 CARRIERS 1 CARRIER_POWER ', 'ANTENNA 3 CARRIERS 1 CARRIER_POWER ']  # Prefixes of the text to be replaced
    # target_text_prefixes = ['ANTENNA 7 CARRIERS 1 CARRIER_POWER ', 'ANTENNA 10 CARRIERS 1 CARRIER_POWER ', 'ANTENNA 2 CARRIERS 1 CARRIER_POWER ']  # Prefixes of the text to be replaced
    target_numbers = ['30.000', '30.000', '30.000']  # Numbers part of the text to be replaced

    print("Current Run:", starting)
    netfile, powerDir= create_single_variant(input_net_filename, input_nup_filename, output_dir, target_text_prefixes, target_numbers, starting,0)
    print(netfile, powerDir)
    errors = run_winprop_cli(netfile)
    netfile1, rsrpDir1= create_single_variant(input_net_filename, input_nup_filename, output_dir, target_text_prefixes, target_numbers, starting,1)
    print(netfile1, rsrpDir1)
    errors = run_winprop_cli(netfile1)
    netfile2, rsrpDir2= create_single_variant(input_net_filename, input_nup_filename, output_dir, target_text_prefixes, target_numbers, starting,2)
    print(netfile2, rsrpDir2)
    errors = run_winprop_cli(netfile2)
    means = calcDiffMean(powerDir, rsrpDir1, rsrpDir2)
    print(means)
    # exit(0)
           


if __name__ == "__main__":
    # Example usage:
    
    # List of replacement numbers with specific precision
    # starting = [
    #     '35.000',  # For Meb-south
    #     '35.000',   # For Wasatch
    #     '30.000'    # For ebc/gh
    #     ]

    starting = ['35.000', '35.000', '30.000']
    
    runVariant(starting)
    
    
    
    
    # runVariations(0)
    # print("Done with Variation: Vertical Tilt")
    # runVariations(1)
    # print("Done with Variation: Power")
    # runVariations(2)
    # print("Done with Variation: Horizontal Tilt")
    # runVariations(3)
    # print("Done with Variation: NLOS Exponent")
    # runVariations(4)
    # print("Done with Variation: NLOS Exponent")
    # runAllVariants()
    

   
    
    