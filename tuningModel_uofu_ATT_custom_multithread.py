import os
import itertools
import subprocess
import pandas as pd
import utm
import glob
import os
import numpy as np
import json
import shutil
import re
import threading
from concurrent.futures import ProcessPoolExecutor
import multiprocessing


def extract_antenna_data(filename, cellid):
    with open(filename, 'r') as file:
        lines = file.readlines()

    antenna_data = {}
    prefixes = []
    values = []
    
    # Define regex patterns to match antenna number and properties
    antenna_pattern = re.compile(r'ANTENNA (\d+) NAME "([A-Za-z0-9\s]+)"')  # Match ANTENNA number and NAME
    vertical_pattern = re.compile(r'ANTENNA (\d+) VERTICAL (\d+\.\d+)')
    horizontal_pattern = re.compile(r'ANTENNA (\d+) HORIZONTAL (\d+\.\d+ [A-Z])')  # Include 'N' in the pattern
    carrier_power_pattern = re.compile(r'ANTENNA (\d+) CARRIERS 1 CARRIER_POWER (\d+\.\d+)')

    # Loop through lines to capture all antennas for the given cellid
    current_antenna = None
    for line in lines:
        # Look for the antenna and its name first
        antenna_match = antenna_pattern.search(line)
        if antenna_match:
            antenna_number = antenna_match.group(1)
            antenna_name = antenna_match.group(2)
            
            if cellid in antenna_name:
                current_antenna = antenna_number  # Update current antenna number based on cellid match

        # Once we've identified the antenna number, gather its properties
        if current_antenna:
            # Find vertical, horizontal, and carrier power for the identified antenna
            vertical_match = vertical_pattern.search(line)
            horizontal_match = horizontal_pattern.search(line)
            carrier_power_match = carrier_power_pattern.search(line)

            # Store matches in the dictionary if they exist
            if vertical_match and vertical_match.group(1) == current_antenna:
                prefixes.append(f'ANTENNA {current_antenna} VERTICAL ')
                values.append(vertical_match.group(2))
            if horizontal_match and horizontal_match.group(1) == current_antenna:
                prefixes.append(f'ANTENNA {current_antenna} HORIZONTAL ')
                values.append(horizontal_match.group(2))
            if carrier_power_match and carrier_power_match.group(1) == current_antenna:
                prefixes.append(f'ANTENNA {current_antenna} CARRIERS 1 CARRIER_POWER ')
                values.append(carrier_power_match.group(2))
                prefixes.append(f'ANTENNA {current_antenna} DISABLED ')
                values.append("y")
    return prefixes, values


ground_truth_df = pd.DataFrame()

def to_radians(degrees):
    return np.radians(degrees)

# Haversine distance function
def haversine(lat1, lon1, lat2, lon2):
    R = 6371000  # Radius of the Earth in meters
    dlat = to_radians(lat2 - lat1)
    dlon = to_radians(lon2 - lon1)
    a = np.sin(dlat / 2) ** 2 + np.cos(to_radians(lat1)) * np.cos(to_radians(lat2)) * np.sin(dlon / 2) ** 2
    c = 2 * np.arctan2(np.sqrt(a), np.sqrt(1 - a))
    return R * c

# Find nearest neighbors using Haversine distance
def find_nearest_neighbors(ground_truth_df, feko_df):
    nearest_indices = []
    for i, filtered_row in ground_truth_df.iterrows():
        distances = haversine(filtered_row['Latitude'], filtered_row['Longitude'], feko_df['Latitude'], feko_df['Longitude'])
        # print(distances.min())
        nearest_index = distances.argmin()
        if distances.min() >5:
        #   print(distances.min())
          nearest_indices.append(-1)
          continue
        nearest_indices.append(nearest_index)
    return nearest_indices


def read_GR(cellid):
    global ground_truth_df
    # df1 = pd.read_csv('/fekoStuff/GR_RUN/qscan_2024-04-18_17:00:05.csv')
    # df2 = pd.read_csv('/fekoStuff/GR_RUN/qscan_2024-04-18_17:03:48.csv')
    # df3 = pd.read_csv('/fekoStuff/GR_RUN/qscan_2024-04-18_17:29:16.csv')

    # combined_df = pd.concat([df1, df2, df3], ignore_index=True)
    # combined_df = pd.read_csv("/fekoStuff/fekovariantexecutor/base_files/GRD_159_bigger.csv")
    print(f"/fekoStuff/fekovariantexecutor/base_files/9820_rsrps/rsrp_{cellid.split(' ')[1]}.csv")
    combined_df = pd.read_csv(f"/fekoStuff/fekovariantexecutor/base_files/9820_rsrps/rsrp_{cellid.split(' ')[1]}.csv")
    ground_truth_df = combined_df.copy()
    # ground_truth_df = combined_df[combined_df['cellid'] == '6E1BB98']

    # ground_truth_df = ground_truth_df[['latitude', 'longitude', 'RSRP']]
    # ground_truth_df['Latitude'] = ground_truth_df["latitude"]
    # ground_truth_df['Longitude'] = ground_truth_df["longitude"]



    ground_truth_df = ground_truth_df.loc[~(ground_truth_df['RSRP'] == '-')]
    ground_truth_df['RSRP'] = ground_truth_df['RSRP'].astype(int)


def error_calc(feko_csv):
    global ground_truth_df
    feko_df = pd.read_csv(feko_csv)
    feko_df = feko_df.loc[~(feko_df['RSRP'] == 'N.C.')]
    feko_df['RSRP'] = feko_df['RSRP'].astype(float)
    feko_df['RSRP'] = feko_df['RSRP'].astype(int)
    nearest_indices = find_nearest_neighbors(ground_truth_df, feko_df)

    # Get the corresponding RSRP values from feko_df
    ground_truth_df_copy = ground_truth_df.copy()

  # Replace -1 indices with NaN in RSRP_feko column
    feko_values = feko_df['RSRP'].values
    RSRP_feko_values = np.where(np.array(nearest_indices) != -1, feko_values[nearest_indices], np.nan)
    ground_truth_df_copy['RSRP_feko'] = RSRP_feko_values

    # Calculate the difference in RSRP
    ground_truth_df_copy['RSRP_diff'] = ground_truth_df_copy['RSRP_feko'] - ground_truth_df_copy['RSRP']

    # Calculate the Mean Percentage Error (MPE)
    ground_truth_df_copy['RSRP_abs'] = np.abs(ground_truth_df_copy['RSRP'])
    ground_truth_df_copy['RSRP_abs_diff'] = np.abs(ground_truth_df_copy['RSRP_diff'])
    ground_truth_df_copy['MPE'] = np.abs(ground_truth_df_copy['RSRP_diff'] / ground_truth_df_copy['RSRP_abs']) * 100
    mpe = np.mean(ground_truth_df_copy['MPE'].replace([np.inf, -np.inf], np.nan).dropna())
    mae = np.mean(ground_truth_df_copy['RSRP_abs_diff'].replace([np.inf, -np.inf], np.nan).dropna())
    std_dev = ground_truth_df_copy['RSRP_diff'].std(ddof=0)
    min_dev = ground_truth_df_copy['RSRP_diff'].min()
    max_dev = ground_truth_df_copy['RSRP_diff'].max()
    print(std_dev)
    print(mpe)

    return std_dev, mpe, min_dev, max_dev




    


# Function to convert UTM to lat lon
def convert_to_lat_lon(x, y, zone_number, zone_letter):
    lat, lon = utm.to_latlon(x, y, zone_number, zone_letter)
    return lat, lon

# Example zone information
zone_number = 12  # Example, please replace with actual zone number
zone_letter = 'N'  # Example, please replace with actual zone letter

def csv_maker(cellid, foldername):
    input_file = f'/fekoStuff/net_variants_multithreaded_{cellid}/{foldername}/RSRP.txt'
    data_points = []
    with open(input_file, 'r') as file:
        for line in file:
            if line.strip() == 'BEGIN_DATA':
                break
        for line in file:
            if line.strip() == 'END_DATA':
                break
            parts = line.strip().split()
            x = float(parts[0])
            y = float(parts[1])
            value = parts[2]
            data_points.append((x, y, value))

    # Create output file name with parent directory name
    print(input_file)
    output_file = os.path.join(f'/fekoStuff/net_variants_multithreaded_{cellid}/{foldername}', f'RSRP.csv')
    print(output_file)
    # # Convert data points to lat lon and create DataFrame
    df_data = []
    for x, y, value in data_points:
        lat, lon = convert_to_lat_lon(x, y, zone_number, zone_letter)
        df_data.append({'Latitude': lat, 'Longitude': lon, 'RSRP': value})

    # Create DataFrame from data points
    df = pd.DataFrame(df_data)

    # Convert DataFrame to CSV
    df.to_csv(output_file, index=False)
    return output_file
def run_winprop_cli(netfile):
    """
    Execute the WinPropCLI command with the specified netfile.
    
    Parameters:
    netfile (str): The path to the netfile to be processed.
    
    Returns:
    dict: A dictionary containing 'success' (bool), 'output' (str), 'error' (str).
    """
    command = [
        '/opt/feko/2022/altair/feko/bin/WinPropCLI', 
        '-A', 
        # '-P', 
        '--multi-threading', 
        '32', 
        '-F', 
        netfile
    ]
    
    try:
        # Execute the command and wait for it to complete
        result = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=False)  # text=False ensures raw byte output
        
        # Decode output using a different encoding (ISO-8859-1) to handle potential encoding issues
        stdout_output = result.stdout.decode('ISO-8859-1', errors='replace')  # Use 'replace' to handle invalid characters
        stderr_output = result.stderr.decode('ISO-8859-1', errors='replace')
        
        # Return result based on execution
        return {
            'success': result.returncode == 0,
            'output': stdout_output if result.returncode == 0 else None,
            'error': stderr_output if result.returncode != 0 else None
        }
    
    except Exception as e:
        return {
            'success': False,
            'output': None,
            'error': str(e)
        }

total_count = 0

def update_values(old_starting, horizontal, vertical, power):
    starting = old_starting.copy()
    try:
        # Parse the first value and apply the increment
        current_value = float(starting[0].split(' N')[0])
        new_value = current_value + horizontal
        
        # Format the new value to two decimal places
        starting[0] = f"{new_value:.2f} N"
        
    except ValueError:
        print("Error: first element in starting list is not a valid number.")
        return starting
    try:
        # Parse the first value and apply the increment
        current_value = float(starting[1])
        new_value = current_value + vertical
        
        # Format the new value to two decimal places
        starting[1] = f"{new_value:.2f}"
        
    except ValueError:
        print("Error: Second element in starting list is not a valid number.")
        return starting
    try:
        # Parse the first value and apply the increment
        current_value = float(starting[2])
        new_value = current_value + power
        
        # Format the new value to two decimal places
        starting[2] = f"{new_value:.3f}"
        
        
    except ValueError:
        print("Error: Third element in starting list is not a valid number.")
        return starting
    starting[3] = "n"
    return starting

def update_horizontal_value(starting, increment):
    # Ensure starting is a list of strings, and increment is either +0.5 or -0.5
    try:
        # Parse the first value and apply the increment
        current_value = float(starting[0].split(' N')[0])
        new_value = current_value + increment
        
        # Format the new value to two decimal places
        starting[0] = f"{new_value:.2f} N"
        
        return starting
    except ValueError:
        print("Error: first element in starting list is not a valid number.")
        return starting

def update_vertical_value(starting, increment):
    # Ensure starting is a list of strings, and increment is either +0.5 or -0.5
    try:
        # Parse the first value and apply the increment
        current_value = float(starting[1])
        new_value = current_value + increment
        
        # Format the new value to two decimal places
        starting[1] = f"{new_value:.2f}"
        
        return starting
    except ValueError:
        print("Error: Second element in starting list is not a valid number.")
        return starting

def update_power_value(starting, increment):
    # Ensure starting is a list of strings, and increment is either +0.5 or -0.5
    try:
        # Parse the first value and apply the increment
        current_value = float(starting[2])
        new_value = current_value + increment
        
        # Format the new value to two decimal places
        starting[2] = f"{new_value:.3f}"
        
        return starting
    except ValueError:
        print("Error: Third element in starting list is not a valid number.")
        return starting

def update_nlos_value(starting, increment):
    # Ensure starting is a list of strings, and increment is either +0.5 or -0.5
    try:
        # Parse the first value and apply the increment
        current_value = float(starting[4])
        new_value = current_value + increment
        
        # Format the new value to two decimal places
        starting[4] = f"{new_value:.3f}"
        
        return starting
    except ValueError:
        print("Error: Second element in starting list is not a valid number.")
        return starting
def update_los_value(starting, increment):
    # Ensure starting is a list of strings, and increment is either +0.5 or -0.5
    try:
        # Parse the first value and apply the increment
        current_value = float(starting[5])
        new_value = current_value + increment
        
        # Format the new value to two decimal places
        starting[5] = f"{new_value:.3f}"
        
        return starting
    except ValueError:
        print("Error: Second element in starting list is not a valid number.")
        return starting

def create_single_variant(input_net_filename, input_nup_filename, output_dir, target_text_prefixes, target_numbers, replacements):
    """
    Create a single variant of .net and .nup files by replacing target texts with new values.

    Args:
        input_net_filename (str): Path to the input .net file.
        input_nup_filename (str): Path to the input .nup file.
        output_dir (str): Directory where output files will be saved.
        target_text_prefixes (list of str): Prefixes for target texts to be replaced.
        target_numbers (list of int): Numbers to combine with prefixes to form target texts.
        replacements (list of str): Replacement values for each target text.

    Returns:
        tuple: Names of the created .net and .nup files.
    """
    # Ensure the output directory exists
    os.makedirs(output_dir, exist_ok=True)

    # Read the content of the input .net file
    net_content = read_file(input_net_filename)
    if net_content is None:
        return None, None

    # Read the content of the input .nup file
    nup_content = read_file(input_nup_filename)
    if nup_content is None:
        return None, None

    # Extract the file base name and extension for .net file
    net_base_name, net_ext = os.path.splitext(os.path.basename(input_net_filename))

    modified_net_content = net_content
    modified_nup_content = nup_content
    filename_parts = []

    for prefix, number, replacement in zip(target_text_prefixes, target_numbers, replacements):
        target_text = f"{prefix}{number}"
        replacement_text = f"{prefix}{replacement}"
        # print(target_text, replacement_text)
        modified_net_content = modified_net_content.replace(target_text, replacement_text)
        modified_nup_content = modified_nup_content.replace(target_text, replacement_text)
        # Prepare part of the filename for this replacement
        # print(replacement)
        filename_parts.append(replacement.replace('.', '_'))

    # Update OUTPUT_PROPAGATION_FILES and OUTPUT_NETWORK_FILES lines in .net file
    prop_replacement = f'OUTPUT_PROPAGATION_FILES "PropNameIRT_{"_".join(filename_parts)}"'
    net_replacement = f'OUTPUT_NETWORK_FILES "NetNameIRT_{"_".join(filename_parts)}"'
    modified_net_content = update_files(modified_net_content, prop_replacement, net_replacement)

    # Create new filenames for the variant .net and .nup files
    net_output_filename = os.path.join(output_dir, f'{net_base_name}_variant_{"_".join(filename_parts)}{net_ext}')
    nup_output_filename = os.path.join(output_dir, f'{net_base_name}_variant_{"_".join(filename_parts)}.nup')

    # Write the modified content to the new .net file
    write_file(net_output_filename, modified_net_content)

    # Write the modified content to the new .nup file
    write_file(nup_output_filename, modified_nup_content)

    return net_output_filename, net_replacement.split("\"")[1]

def read_file(filename):
    """Read the content of a file."""
    try:
        with open(filename, 'r') as file:
            return file.read()
    except FileNotFoundError:
        print(f"Error: The file '{filename}' does not exist.")
        return None

def write_file(filename, content):
    """Write content to a file."""
    with open(filename, 'w') as file:
        file.write(content)

def update_files(net_content, prop_replacement, net_replacement):
    """Update specific lines in the .net file content."""
    lines = net_content.splitlines()
    updated_lines = []
    for line in lines:
        if line.startswith('OUTPUT_PROPAGATION_FILES'):
            updated_lines.append(prop_replacement)
        elif line.startswith('OUTPUT_NETWORK_FILES'):
            updated_lines.append(net_replacement)
        else:
            updated_lines.append(line)
    return '\n'.join(updated_lines)



def runAllVariants(cellid):
    input_net_filename = '/fekoStuff/fekovariantexecutor/base_files/base_IRT_uofu_ATT.net'  # Path to the input .net file
    input_nup_filename = '/fekoStuff/fekovariantexecutor/base_files/base_IRT_uofu_ATT.nup'  # Path to the input .nup file
    output_dir = '/fekoStuff/net_variants_multithreaded_' + cellid  # Directory to store the output files
    # target_text_prefixes = ['ANTENNA 1 VERTICAL ', 'ANTENNA 1 CARRIERS 1 CARRIER_POWER ', 'ANTENNA 1 HORIZONTAL ']  # Prefixes of the text to be replaced
    # target_numbers = ['2.00', '42.600', '55.00 N']  # Numbers part of the text to be replaced
    target_text_prefixes, target_numbers = extract_antenna_data(input_net_filename, cellid)
    starting_initial = update_values(target_numbers,-20,-2,-3) 
    starting = starting_initial.copy()
    ending = update_values(target_numbers,20, 5, 5) 
    print(target_text_prefixes ,target_numbers)
    print(starting_initial, ending)

    std_dev_dict = {}
    file_path = f'mixed_{cellid}.json'
    try:
        with open(file_path, 'r') as file:
            std_dev_dict = json.load(file)
            print("File read successfully")
    except FileNotFoundError:
        print("File Error")
    while starting[0]!=ending[0]:
        while starting[1] != ending[1]:
            while starting[2] != ending[2]:
                print("Current Run:", starting)
                if str(starting) in std_dev_dict:
                    print("Already Present")
                    update_power_value(starting, 1)
                    continue
                read_GR(cellid)
                netfile, rsrpDir= create_single_variant(input_net_filename, input_nup_filename, output_dir, target_text_prefixes, target_numbers, starting)
                print(netfile, rsrpDir)
                errors = run_winprop_cli(netfile)
                feko_csv = csv_maker(cellid, rsrpDir)
                std_dev, mpe, min_dev, max_dev = error_calc(feko_csv)
                try:
                    with open(file_path, 'r') as file:
                        std_dev_dict = json.load(file)
                        print("File read successfully")
                except FileNotFoundError:
                    print("File Error")
                std_dev_dict[str(starting)] = (std_dev, mpe, min_dev, max_dev)
                with open(file_path, 'w') as file:
                    json.dump(std_dev_dict, file, indent=4)
                update_power_value(starting, 1)
            starting[2] = starting_initial[2]
            update_vertical_value(starting, 1)
            print("Deleting Direcoty")
            shutil.rmtree(output_dir)
        starting[1] = starting_initial[1]
        update_horizontal_value(starting, 5)
        
       
def process_inner_loop_safe(args):
    """Wrapper function to unpack arguments and call process_inner_loop."""
    process_inner_loop(*args)

def process_inner_loop(starting, std_dev_dict, lock, cellid, input_net_filename, input_nup_filename, output_dir, target_text_prefixes, target_numbers, starting_initial):
    print("Current Run:", starting)
    
    netfile, rsrpDir = create_single_variant(input_net_filename, input_nup_filename, output_dir, target_text_prefixes, target_numbers, starting)
    print(netfile, rsrpDir)
    errors = {'error': None, 'success': False}
    
    # while errors['success'] == False:
    errors = run_winprop_cli(netfile)
        # print(errors['success'], erroxrs['error'])
    feko_csv = csv_maker(cellid, rsrpDir)
    std_dev, mpe, min_dev, max_dev = error_calc(feko_csv)

    with lock:
        print("Acquired lock")
        std_dev_dict[str(starting)] = (std_dev, mpe, min_dev, max_dev)

def runAllPathLossVariants(cellid):
    input_net_filename = '/fekoStuff/fekovariantexecutor/base_files/base_IRT_uofu_ATT.net'
    input_nup_filename = '/fekoStuff/fekovariantexecutor/base_files/base_IRT_uofu_ATT.nup'
    output_dir = f'/fekoStuff/net_variants_multithreaded_{cellid}'
    file_path = f'mixed_{cellid}_multiThreaded.json'

    try:
        with open(file_path, 'r') as file:
            std_dev_dict = json.load(file)
    except FileNotFoundError:
        std_dev_dict = {}

    manager = multiprocessing.Manager()
    std_dev_dict = manager.dict(std_dev_dict)
    lock = manager.Lock()

    target_text_prefixes, target_numbers = extract_antenna_data(input_net_filename, cellid)
    target_text_prefixes.extend(['EXPONENT_BEFORE_BREAKPOINT ', 'EXPONENT_BEFORE_BREAKPOINT_LOS '])
    target_numbers.extend(['2.700', '2.500'])

    starting_initial = update_values(target_numbers, -20, -2, -3)
    starting_initial[4] = '2.000'
    starting_initial[5] = '2.000'
    starting = starting_initial.copy()
    ending = update_values(target_numbers, 20, 5, 5)
    ending[4] = '2.900'
    ending[5] = '2.900'

    read_GR(cellid)
    workers = 32
    count = 0
    with ProcessPoolExecutor(max_workers=workers) as executor:
        while starting[0] != ending[0]:
            while starting[1] != ending[1]:
                while starting[2] != ending[2]:
                    while starting[4] != ending[4]:
                        print("Current NLOS Value:", starting[4])
                        while starting[5] != ending[5]:
                            if float(starting[5]) > float(starting[4]):
                                break

                            with lock:
                                if str(starting) in std_dev_dict:
                                    print("Already Calculated:", str(starting))
                                    update_los_value(starting, 0.1)
                                    continue

                            args = (starting[:], std_dev_dict, lock, cellid, input_net_filename, input_nup_filename, output_dir, target_text_prefixes, target_numbers, starting_initial)
                            executor.submit(process_inner_loop_safe, args)
                            update_los_value(starting, 0.1)

                            count += 1
                            if count == workers:
                                executor.shutdown(wait=True)
                                count = 0
                                
                                with open(file_path, 'w') as file:
                                    with lock:
                                        json.dump(std_dev_dict.copy(), file, indent=4)

                                if os.path.exists(output_dir):
                                    shutil.rmtree(output_dir)

                                executor = ProcessPoolExecutor(max_workers=workers)

                        starting[5] = starting_initial[5]
                        update_nlos_value(starting, 0.1)

                    starting[4] = starting_initial[4]
                    update_power_value(starting, 1)

                starting[2] = starting_initial[2]
                update_vertical_value(starting, 1)

            starting[1] = starting_initial[1]
            update_horizontal_value(starting, 5)

    with open(file_path, 'w') as file:
        json.dump(std_dev_dict.copy(), file, indent=4)


def runAllPathLossVariants_guess(cellid):
    input_net_filename = '/fekoStuff/fekovariantexecutor/base_files/base_IRT_uofu_ATT.net'
    input_nup_filename = '/fekoStuff/fekovariantexecutor/base_files/base_IRT_uofu_ATT.nup'
    output_dir = f'/fekoStuff/net_variants_multithreaded_{cellid}'
    file_path = f'/fekoStuff/fekovariantexecutor/9820_guess_results/mixed_{cellid}_multiThreaded_guess.json'

    try:
        with open(file_path, 'r') as file:
            std_dev_dict = json.load(file)
    except FileNotFoundError:
        std_dev_dict = {}

    manager = multiprocessing.Manager()
    std_dev_dict = manager.dict(std_dev_dict)
    lock = manager.Lock()

    target_text_prefixes, target_numbers = extract_antenna_data(input_net_filename, cellid)
    target_text_prefixes.extend(['EXPONENT_BEFORE_BREAKPOINT ', 'EXPONENT_BEFORE_BREAKPOINT_LOS '])
    target_numbers.extend(['2.700', '2.500'])

    starting_initial = update_values(target_numbers, -310, 0, -1)
    starting_initial[4] = '2.000'
    starting_initial[5] = '2.000'
    starting = starting_initial.copy()
    ending = update_values(target_numbers, 40, 8, 6)
    ending[4] = '2.900'
    ending[5] = '2.900'

    read_GR(cellid)
    workers = 32
    count = 0
    with ProcessPoolExecutor(max_workers=workers) as executor:
        while starting[0] != ending[0]:
            while starting[1] != ending[1]:
                while starting[2] != ending[2]:
                    while starting[4] != ending[4]:
                        print("Current NLOS Value:", starting[4])
                        while starting[5] != ending[5]:
                            if float(starting[5]) > float(starting[4]):
                                break

                            with lock:
                                if str(starting) in std_dev_dict:
                                    print("Already Calculated:", str(starting))
                                    update_los_value(starting, 0.1)
                                    continue

                            args = (starting[:], std_dev_dict, lock, cellid, input_net_filename, input_nup_filename, output_dir, target_text_prefixes, target_numbers, starting_initial)
                            executor.submit(process_inner_loop_safe, args)
                            update_los_value(starting, 0.1)

                            count += 1
                            if count == workers:
                                executor.shutdown(wait=True)
                                count = 0
                                
                                with open(file_path, 'w') as file:
                                    with lock:
                                        json.dump(std_dev_dict.copy(), file, indent=4)

                                if os.path.exists(output_dir):
                                    shutil.rmtree(output_dir)

                                executor = ProcessPoolExecutor(max_workers=workers)

                        starting[5] = starting_initial[5]
                        update_nlos_value(starting, 0.1)

                    starting[4] = starting_initial[4]
                    update_power_value(starting, 1)

                starting[2] = starting_initial[2]
                update_vertical_value(starting, 1)

            starting[1] = starting_initial[1]
            update_horizontal_value(starting, 5)

    with open(file_path, 'w') as file:
        json.dump(std_dev_dict.copy(), file, indent=4)

def run_single(starting, cellid, input_net_filename, input_nup_filename, output_dir, target_text_prefixes, target_numbers, starting_initial):
    print("Current Run:", starting)
    
    netfile, rsrpDir = create_single_variant(input_net_filename, input_nup_filename, output_dir, target_text_prefixes, target_numbers, starting)
    print(netfile, rsrpDir)
    errors = {'error': None, 'success': False}
    
    # while errors['success'] == False:
    errors = run_winprop_cli(netfile)
        # print(errors['success'], erroxrs['error'])
    feko_csv = csv_maker(cellid, rsrpDir)
    std_dev, mpe, min_dev, max_dev = error_calc(feko_csv)

    with lock:
        print("Acquired lock")
        std_dev_dict[str(starting)] = (std_dev, mpe, min_dev, max_dev)

def run_single(cellid, starting):
    input_net_filename = '/fekoStuff/fekovariantexecutor/base_files/base_IRT_uofu_ATT.net'
    input_nup_filename = '/fekoStuff/fekovariantexecutor/base_files/base_IRT_uofu_ATT.nup'
    output_dir = f'/fekoStuff/net_variants_multithreaded_{cellid}'
    file_path = f'/fekoStuff/fekovariantexecutor/9820_guess_results/mixed_{cellid}_multiThreaded_guess.json'
    target_text_prefixes, target_numbers = extract_antenna_data(input_net_filename, cellid)
    target_text_prefixes.extend(['EXPONENT_BEFORE_BREAKPOINT ', 'EXPONENT_BEFORE_BREAKPOINT_LOS '])
    target_numbers.extend(['2.700', '2.500'])
    
    
    run_single(starting, cellid, input_net_filename, input_nup_filename, output_dir, target_text_prefixes, target_numbers)

if __name__ == "__main__":
    # Example usage:
    
    # List of replacement numbers with specific precision

    
    
    
    # runVariations(0)
    # print("Done with Variation: Vertical Tilt")
    # runVariations(1)
    # print("Done with Variation: Power")
    # runVariations(2)
    # print("Done with Variation: Horizontal Tilt")
    # runVariations(3)
    # print("Done with Variation: NLOS Exponent")
    # runVariations(4)
    # print("Done with Variation: NLOS Exponent")
    # runAllVariants()
    # runAllPathLossVariants()
    # runAllVariants("6E1DF95 159")
    # runAllPathLossVariants("6E1DF95 159")
    # runAllPathLossVariants("6E1BD95 240")
    data_list = [
        # "6E5B395 39",
        # "6E5C097 77",
        # "6E65095 81",
        # "6E1D095 147",
        # "6E5C295 150",
        # "6E1DF95 159",
        # "6E1BB98 167",
        # "6E1F795 180",
        # "6E26B95 210",
        # "6E5DF95 225",
        # "6E1BD95 240",
        # "6E26095 243",
        # "6E22695 309",
        "6E22697 311",
        # "6E1BB95 334",
        # "6E2D097 416",
        # "6E21E95 450",
        # "6E20D96 355",
        # "6E2EE98 359",
        # "6E1BF95 153",
        # "6E1D495 276",
        # "6E1C795 294"
    ]

    # Loop through the data and apply the function to each string
    for data in data_list:
        runAllPathLossVariants_guess(data)

    
    # runAllPathLossVariants_guess("6E1F795 180")

   
    
    